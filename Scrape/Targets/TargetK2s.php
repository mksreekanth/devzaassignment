<?php
/**
 * Created by PhpStorm.
 * User: sreekanthmk
 * Date: 2020-12-08
 * Time: 21:24
 */
namespace  Scrape\Targets;
final class TargetK2s implements TargetInterface
{

    const LOGIN_URL = "https://k2s.cc/auth/login";
    const LOGIN_USERNAME = "sreekanth.mku@gmail.com";
    const LOGIN_PASSWORD = "Sreekanth123@";
    const LOGIN_ACTION_URL = "https://api.k2s.cc/v1/auth/token";

    final public function getUsername()
    {
        return self::LOGIN_USERNAME;
    }

    final public function getPassword()
    {
        return self::LOGIN_PASSWORD;
    }

    final public function getLoginUrl()
    {
        return self::LOGIN_URL;
    }

    final public function getLoginActionUrl()
    {
        return self::LOGIN_ACTION_URL;
    }

    final public function getData()
    {
        $data = [
            'username' => $this->getUsername(),
            'password' => $this->getPassword(),
            'client_id' => "k2s_web_app",
            'client_secret' => 'pjc8pyZv7vhscexepFNzmu4P',
            'grant_type' => "password",

        ];
        return $data;
    }
}