<?php
/**
 * Created by PhpStorm.
 * User: sreekanthmk
 * Date: 2020-12-08
 * Time: 22:24
 */

namespace Scrape\Targets;


interface TargetInterface
{

    public function getUsername();

    public function getPassword();

    public function getLoginUrl();

    public function getLoginActionUrl();

    public function getData();

}