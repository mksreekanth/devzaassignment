<?php
/**
 * Created by PhpStorm.
 * User: sreekanthmk
 * Date: 2020-12-08
 * Time: 12:16
 */

namespace Scrape;

class ScrapeClient
{
    const OPT_URL = CURLOPT_URL;
    const OPT_POST = CURLOPT_POST;
    const OPT_POSTFIELDS = CURLOPT_POSTFIELDS;
    const OPT_SSL_VERIFYHOST = CURLOPT_SSL_VERIFYHOST;
    const OPT_SSL_VERIFYPEER = CURLOPT_SSL_VERIFYPEER;
    const OPT_COOKIEJAR = CURLOPT_COOKIEJAR;
    const OPT_USERAGENT = CURLOPT_USERAGENT;
    const OPT_RETURNTRANSFER = CURLOPT_RETURNTRANSFER;
    const OPT_REFERER = CURLOPT_REFERER;
    const OPT_FOLLOWLOCATION = CURLOPT_FOLLOWLOCATION;

    private $setOptionsSupported = [
        self::OPT_URL,
        self::OPT_POST,
        self::OPT_POSTFIELDS,
        self::OPT_SSL_VERIFYHOST,
        self::OPT_SSL_VERIFYPEER,
        self::OPT_COOKIEJAR,
        self::OPT_USERAGENT,
        self::OPT_RETURNTRANSFER,
        self::OPT_REFERER,
        self::OPT_FOLLOWLOCATION,
    ];

    private $options = [];


    private $curl;

    public function __construct()
    {
        $this->init();
    }

    private function init(){
        $this->curl = curl_init();
    }

    final public function setOption($option, $value){
        if (!in_array($option, $this->setOptionsSupported)){
            $errorMsg = sprintf("Unable to set option %s", $option);
            trigger_error( $errorMsg, E_USER_WARNING);
            return;
        }
        $this->options[$option] = $value;
        curl_setopt($this->curl, $option, $value);
    }

    final public function execute(){
        if (!isset($this->options[self::OPT_URL])){
            trigger_error( "No Url found to scrape", E_USER_WARNING);
            return;
        }
        $result = curl_exec($this->curl);
        if(curl_errno($this->curl)){
            echo (curl_error($this->curl));
        }
        return $result;
    }
}