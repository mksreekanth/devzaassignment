<?php
/**
 * Created by PhpStorm.
 * User: sreekanthmk
 * Date: 2020-12-08
 * Time: 22:38
 */

namespace Scrape;

use Scrape\Targets\TargetInterface as TargetInterface;
use Scrape\ScrapeClient as ScrapeClient;

class Scrapper
{

    const USER_AGENT = "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.2309.372 Safari/537.36')";

    const COOKIE_FILE_PATH = "cookie.txt";

    private $scrapeTarget;

    private $scrapeClient;

    public function __construct(TargetInterface $target)
    {
        $this->scrapeTarget = $target;
        $this->init();
    }

    private function init(){
        $this->initClient();
        $this->setUrl();
        $this->setData();
    }

    public function execute(){
        return $this->scrapeClient->execute();
    }

    private function initClient(){
        $this->scrapeClient = new ScrapeClient();

        $this->scrapeClient->setOption(ScrapeClient::OPT_POST, true);
        $this->scrapeClient->setOption(ScrapeClient::OPT_SSL_VERIFYHOST, false);
        $this->scrapeClient->setOption(ScrapeClient::OPT_SSL_VERIFYPEER, false);
        $this->scrapeClient->setOption(ScrapeClient::OPT_RETURNTRANSFER, true);
        $this->scrapeClient->setOption(ScrapeClient::OPT_FOLLOWLOCATION, false);
        $this->setUserAgent();
        $this->setCookiePath();
    }

    private function setUserAgent(){
        $this->scrapeClient->setOption(ScrapeClient::OPT_USERAGENT, self::USER_AGENT);
    }

    private function setCookiePath(){
        $this->scrapeClient->setOption(ScrapeClient::OPT_COOKIEJAR, self::COOKIE_FILE_PATH);
    }

    private function setUrl(){
        $this->scrapeClient->setOption(ScrapeClient::OPT_REFERER, $this->scrapeTarget->getLoginUrl());
        $this->scrapeClient->setOption(ScrapeClient::OPT_URL, $this->scrapeTarget->getLoginActionUrl());
    }

    private function setData(){
        $data = $this->scrapeTarget->getData();
        //csrf needs to be set.
        $this->scrapeClient->setOption(ScrapeClient::OPT_POSTFIELDS, http_build_query($data));
    }
}