<?php
/**
 * Created by PhpStorm.
 * User: sreekanthmk
 * Date: 2020-12-08
 * Time: 12:14
 */


spl_autoload_register(function($className) {
    $fileName = str_replace("\\", '/', $className) . ".php";
    if (file_exists($fileName)) {
        include($fileName);
        if (class_exists($className)) {
            return true;
        }
    }
    return false;
});


$targetK2s = new Scrape\Targets\TargetK2s();
$scraper = new Scrape\Scrapper($targetK2s);
echo  $scraper->execute();
